const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const User = require('./models/User');
const users = require('./app/users');
const Message = require('./models/Message');

const app = express();

app.use(express.json());

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};

app.ws('/messenger',async (ws, req) => {
    try {
        const token = req.query.token;

        const user = await User.findOne({token});

        if (!user) return console.log('User not found!');

        console.log('Client connected! User: ', user?.username);
        activeConnections[user?.username] = ws;

        ws.send(JSON.stringify({
            type: 'CONNECTED',
            messages: await Message.find().sort({datetime: -1}).limit(30),
            users: Object.keys(activeConnections),
            user: user.username,
        }));

        ws.on('message' ,async msg => {
            const decode = JSON.parse(msg);
            switch (decode.type) {
                case 'CREATE_MESSAGE':
                    try {
                        const messageData = {
                            message: decode.message.text,
                            datetime: decode.message.datetime,
                            user: user.username,
                        }
                        const message = new Message(messageData);
                        await message.save();

                        Object.keys(activeConnections).forEach(connId => {
                            const conn = activeConnections[connId];

                            conn.send(JSON.stringify({
                                type: 'NEW_MESSAGE',
                                message,
                            }));
                        });
                    } catch (e) {
                        Object.keys(activeConnections).forEach(connId => {
                            const conn = activeConnections[connId];

                            conn.send(JSON.stringify({
                                type: 'USER_ERROR',
                                error: e
                            }));
                        });
                    }
                    break;
                default:
                    console.log('Unknown message type: ', decode.type);
            }
        });

        ws.on('close', msg => {
            delete activeConnections[user?.username];
            console.log('disconnect', activeConnections);
        });

    } catch (e) {
        ws.send(JSON.stringify({
            type: 'ERROR',
            error: e
        }));
    }
});

app.use('/users', users);

const run = async () => {
    await mongoose.connect('mongodb://localhost/messenger');

    app.listen(port, () => {
        console.log('Server works on port: ', port);
    });

    exitHook(() => {
        console.log('Exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));

