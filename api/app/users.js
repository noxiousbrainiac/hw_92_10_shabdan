const express = require('express');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
   try {
       const user = new User({
           username: req.body.username,
           password: req.body.password
       });

       user.generateToken();
       await user.save();
       res.send(user);
   } catch (e) {
       res.status(500).send(e);
   }
});

router.post('/session', async (req, res) => {
    try {
        const user = await User.findOne({username: req.body.username});

        if (!user) return res.status(401).send({message: 'Credentials are wrong!'});

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) return res.status(401).send({message: 'Credentials are wrong!'});

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send({message: "Username and password are correct!", user});
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/session', async  (req, res) => {
   try {
       const token = req.get('Authorization');
       const success = {message: 'Success'};

       if (!token) return res.send(success);

       const user = await User.findOne({token});

       if (!user) return res.send(success);

       await user.save({validateBeforeSave: false});

       return res.send(success);
   } catch (e) {
       res.status(500).send(e);
   }
});

module.exports = router;