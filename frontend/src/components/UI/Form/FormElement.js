import React from 'react';
import {Grid, TextField} from "@material-ui/core";
import PropTypes from 'prop-types';


const FormElement = ({label, name, value, onChange, required, error, autoComplete, type}) => {
    return (
        <Grid item xs={12}>
            <TextField
                autoComplete={autoComplete}
                label={label}
                required={required}
                name={name}
                value={value}
                onChange={onChange}
                error={Boolean(error)}
                helperText={error}
                type={type}
            />
        </Grid>
    );
};

FormElement.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    required: PropTypes.bool,
    error: PropTypes.string,
    autoComplete: PropTypes.string,
    type: PropTypes.string
};

export default FormElement;