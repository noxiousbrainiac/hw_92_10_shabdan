import React from 'react';
import {Grid, makeStyles, Toolbar} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";
import UserMenu from "./Menu/UserMenu";
import Anonymous from "./Menu/Anonymous";

const useStyles = makeStyles({
    Toolbar: {
        background: "#1976d2",
        justifyContent: "space-between"
    },
    title: {
        color: "beige",
        margin: "0 0 0 5px",
        textDecoration: "none",
        fontSize: "20px"
    },
    btn: {
        color: "beige",
    }
})

const AppToolbar = () => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    return (
        <Toolbar className={classes.Toolbar}>
            <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <NavLink to='/' className={classes.title}>
                        Chat
                    </NavLink>
                </Grid>
                <Grid item>
                    {
                        user ? (
                            <UserMenu user={user}/>
                        ) : (
                            <Anonymous/>
                        )
                    }
                </Grid>
            </Grid>
        </Toolbar>
    );
};

export default AppToolbar;