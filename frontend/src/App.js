import React from 'react';
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Chat from "./containers/Chat/Chat";
import {useSelector} from "react-redux";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <ProtectedRoute
                    exact
                    path='/'
                    component={Chat}
                    isAllowed={user}
                    redirectTo='/login'
                />
                <Route path='/register' component={Register}/>
                <Route path='/login' component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;