import React, {useEffect, useRef, useState} from 'react';
import {Button, Grid, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import moment from "moment";
import FormElement from "../../components/UI/Form/FormElement";
import {useDispatch, useSelector} from "react-redux";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

const Chat = () => {
    const ws = useRef(null);
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();
    const restart = useRef(null);

    const [state, setState] = useState({
        text: "",
        messages: [],
        users: [],
        userError: null
    });

    const inputHandler = (e) => {
        const {name, value} = e.target;
        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const send = () => {
        ws.current.send(JSON.stringify({
            type: 'CREATE_MESSAGE',
            message: {
                text: state.text,
                datetime: moment().format('MMMM Do YYYY, h:mm:ss a')
            }
        }));
    };

    const wsCases = (decode) => {
        switch (decode.type) {
            case "CONNECTED":
                setState(prevState => ({
                    ...prevState,
                    messages: decode.messages,
                    users: decode.users,
                    user: decode.user,
                }));
                break;
            case "NEW_MESSAGE":
                setState(prevState => ({
                    ...prevState,
                    text: '',
                    messages: [decode.message, ...prevState.messages],
                }));
                break;
            case "USER_ERROR":
                setState(prevState => ({
                    ...prevState,
                    userError: decode.error.errors.message.message
                }));
                break;
            default: return console.log('Unknown case');
        }
    }

    useEffect(() => {
        ws.current = new WebSocket(`ws://localhost:8000/messenger?token=${user?.token}`);

        ws.current.onmessage = (e) => {
            const decode = JSON.parse(e.data);
            console.log(decode);
            wsCases(decode);
        };

        ws.current.onclose = () => {
            ws.current = null;
            restart.current = setInterval(() => {
                ws.current = new WebSocket(`ws://localhost:8000/messenger?token=${user?.token}`);
                console.log('...Restarting!');
                ws.current.onmessage = (e) => {
                    const decode = JSON.parse(e.data);
                    wsCases(decode);
                    clearInterval(restart.current);
                }
            },5000);
            console.log('disconnected')
        }

        return () => {
            ws.current.close();
            setState(prevState => ({
                ...prevState,
                userError: null,
                users: [],
                messages: []
            }));
        }
    }, [dispatch, user?.token]);

    const getFieldError = fieldName => {
        try {
            return state.userError;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <>
            <Grid container direction="row" style={{paddingTop: "10px"}}>
                <Grid item lg={3} md={3} xs={12}>
                    <List subheader={<b>{'Online users'}</b>}>
                        {state.users.length !== 0
                            ? state.users.map((item, i) => (
                                <ListItem key={i}>
                                    <ListItemIcon>
                                        <AccountCircleIcon/>
                                    </ListItemIcon>
                                    <ListItemText
                                        primary={`${item}`}
                                    />
                                </ListItem>
                            )): null}
                    </List>
                </Grid>
                <Grid item lg={9} md={9} xs={12}>
                    <List style={{overflowY: "scroll", height: "580px", display: "flex",flexDirection: "column-reverse"}}>
                        {state.messages.length !== 0
                            ? state.messages.map((item, i) => (
                                item.user === user.username
                                    ? <ListItem
                                        key={i}
                                        style={{
                                            textAlign: "right",
                                            marginBottom: "5px",
                                            background: "#5699e3"
                                        }}>
                                        <ListItemText
                                            primary={item.message}
                                            secondary={`You - ${item.datetime}`}
                                        />
                                    </ListItem>
                                    : <ListItem
                                        key={i}
                                        style={{
                                            marginBottom: "5px",
                                            background: "#b5d0ea"
                                        }}>
                                        <ListItemText
                                            primary={item.message}
                                            secondary={`${item.user} - ${item.datetime}`}
                                        />
                                    </ListItem>
                            )) : null
                        }
                    </List>
                </Grid>
            </Grid>
            <Grid container justifyContent="center" alignItems="center" style={{paddingTop: "10px"}} spacing={2}>
                <Grid item lg={6} md={6} xs={10}>
                    <FormElement
                        label='Message'
                        onChange={e => inputHandler(e)}
                        name='text'
                        value={state.text}
                        error={getFieldError('text')}
                    />
                </Grid>
                <Grid item lg={2} md={2} xs={2}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={send}
                    >
                        Send
                    </Button>
                </Grid>
            </Grid>
        </>
    );
};

export default Chat;